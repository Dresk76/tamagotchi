using System.Collections;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [Header("LAUNCH")]
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private Transform launchPoint;
    [SerializeField] private float launchForce = 10f;
    [SerializeField] private float launchTime = 0.8f;
    private bool _canLaunch = true;


    [Header("MOVEMENT PLAYER")]
    [SerializeField] private float timeMove = 1.2f;


    [Header("COLOR")]
    [SerializeField] private Color colorBall;


    [Header("BALL")]
    private GameObject _ballPrefab;


    [Header("REFERENCES")]
    [SerializeField] private PlayerController playerController;
    [SerializeField] private Animator animator;
    [SerializeField] private UIManager uiManager;




    //--> Getters and Setters
    public bool CanLaunch
    {
        get { return _canLaunch; }
        set { _canLaunch = value; }
    }

    public Color ColorBall
    {
        get { return colorBall; }
        set { colorBall = value; }
    }

    public GameObject BallPrefab
    {
        get { return _ballPrefab; }
    }



    //--> Change the color of the ball in the UI.
    private void ChangeColor()
    {
        uiManager.ChangeBallIconColor(colorBall);
    }



    //--> Performs animation actions.
    private void AnimationAction()
    {
        animator.SetBool("CanLaunch", _canLaunch);
        animator.SetTrigger("Throw");
    }



    //--> Only if a ball is allowed to be launched (_canLaunch) Instances a ball in the player's hand and launches it.
    private void ThrowBall()
    {
        if (_canLaunch)
        {
            _ballPrefab = Instantiate(ballPrefab, launchPoint.position, launchPoint.rotation);
            Rigidbody ballRb = _ballPrefab.GetComponent<Rigidbody>();
            ballRb.AddForce(launchPoint.forward * launchForce, ForceMode.Impulse);
            _canLaunch = false;
        }
    }



    //--> Perform the complete ball launching process.
    public IEnumerator BallThrowing()
    {
        playerController.CanMove = false;
        ChangeColor();
        AnimationAction();
        yield return new WaitForSeconds(launchTime);
        ThrowBall();

        yield return new WaitForSeconds(timeMove);
        playerController.CanMove = true;
    }
}
