using UnityEngine;

public class OpeningDoor : MonoBehaviour
{
    [Header("REFERENCES")]
    private Animator _animator;



    private void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
    }



    //--> Open the door when the player or dog enters the trigger.
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player") || collider.CompareTag("Dog"))
        {
            _animator.SetBool("openDoor", true);
        }
    }


    private void OnTriggerStay(Collider collider)
    {
        if (collider.CompareTag("Player") || collider.CompareTag("Dog"))
        {
            _animator.SetBool("openDoor", true);
        }
    }



    //--> Close the door when the player or dog leaves the trigger.
    private void OnTriggerExit(Collider collider)
    {
        if (collider.CompareTag("Player") || collider.CompareTag("Dog"))
        {
            _animator.SetBool("openDoor", false);
        }
    }
}
