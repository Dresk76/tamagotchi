using UnityEngine;

public class DisableObject : MonoBehaviour
{
    [Header("BALL")]
    private Ball _ball;


    [Header("COLOR")]
    private Color colorObject = Color.white;


    [Header("REFERENCES")]
    private UIManager _uiManager;



    private void Awake()
    {
        _ball = GameObject.Find("ObjectsRequired").GetComponentInChildren<Ball>();
        _uiManager = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
    }


    //--> Deactivate object when dog comes into contact with its trigger.
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Dog"))
        {
            Destroy(gameObject);
            ResetBallLaunch();
        }
    }



    private void ResetBallLaunch()
    {
        _ball.CanLaunch = true;
        _uiManager.ChangeBallIconColor(colorObject);
    }
}
