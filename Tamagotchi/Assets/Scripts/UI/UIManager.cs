using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("HEALTH")]
    [SerializeField] private GameObject[] health;


    [Header("LOVE")]
    [SerializeField] private GameObject[] love;


    [Header("PLAY")]
    [SerializeField] private GameObject[] play;


    [Header("FEED")]
    [SerializeField] private GameObject[] feed;


    [Header("PET NAME")]
    [SerializeField] private TMP_Text petName;
    [SerializeField] private GameObject buttonContinue;


    [Header("BALL")]
    [SerializeField] private Image ballIcon;



    //--> Getters
    public GameObject[] Love
    {
        get { return love; }
    }

    public GameObject[] Play
    {
        get { return play; }
    }

    public GameObject[] Feed
    {
        get { return feed; }
    }

    public TMP_Text PetName
    {
        get { return petName; }
    }



    //--> Deactivate a life in the UI.
    public void DeactivateHealth(int index)
    {
        health[index].SetActive(false);
    }


    //--> Activate a life in the UI.
    public void ActivateHealth(int index)
    {
        for (int i = 0; i < index; i++)
        {
            health[i].SetActive(true);
        }
    }


    //--> Disables a love value in the UI.
    public void DeactivateLove(int index)
    {
        love[index].SetActive(false);
    }


    //--> Activates a love value in the UI.
    public void ActivateLove(int index)
    {
        for (int i = 0; i < index; i++)
        {
            love[i].SetActive(true);
        }
    }


    //--> Disables a play value in the UI.
    public void DeactivatePlay(int index)
    {
        play[index].SetActive(false);
    }


    //--> Activates a play value in the UI.
    public void ActivatePlay(int index)
    {
        for (int i = 0; i < index; i++)
        {
            play[i].SetActive(true);
        }
    }


    //--> Disables a food value in the UI.
    public void DeactivateFood(int index)
    {
        feed[index].SetActive(false);
    }


    //--> Activates a food value in the UI.
    public void ActivateFood(int index)
    {
        for (int i = 0; i < index; i++)
        {
            feed[i].SetActive(true);
        }
    }


    //--> Modify the dog's name.
    public void SetInputText(string inputText)
    {
        petName.text = inputText;
        buttonContinue.SetActive(true);
    }


    //--> Change the color of the FunButton icon.
    public void ChangeBallIconColor(Color color)
    {
        ballIcon.color = color;
    }
}
