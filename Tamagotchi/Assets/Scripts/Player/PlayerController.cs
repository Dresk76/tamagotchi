using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("MOVEMENT VARIABLES")]
    [SerializeField] private float _moveSpeed = 4f;
    [SerializeField] private float _rotationSmooth = 0.2f;
    private Vector3 _moveAxis;
    private Vector3 _camForward, _camRight;
    private Vector3 _moveDir, _movement;


    [Header("CONDITIONS")]
    private bool _canMove;


    [Header("REFERENCES")]
    private CharacterController _characterController;
    private Animator _animator;
    private Transform _camera;




    //--> Getters and Setters
    public bool CanMove
    {
        get { return _canMove; }
        set { _canMove = value; }
    }




    void Awake()
    {
        _characterController = GetComponentInChildren<CharacterController>();
        _animator = GetComponentInChildren<Animator>();
        _camera = Camera.main.transform;
    }



    private void Update()
    {
        Move();
        MovementAnimation();
    }



    //--> Character movement.
    private void Move()
    {
        //--> Take inputs entered by the player only if it can move.
        float movementHorizontal = Input.GetAxis("Horizontal");
        float movementVertical = Input.GetAxis("Vertical");

        _moveAxis = new Vector3(movementHorizontal, 0f, movementVertical);


        //--> Normalize vectors.
        if (_moveAxis.magnitude > 1)
        {
            _moveAxis = _moveAxis.normalized * _moveSpeed;
        }
        else
        {
            _moveAxis *= _moveSpeed;
        }


        //--> Fulfilled if user inputs are different from zero.
        if (movementHorizontal != 0 || movementVertical != 0)
        {
            CameraDirection();
            _moveDir = _moveAxis.x * _camRight + _moveAxis.z * _camForward;

            //--> Assign movement value only if it can be moved.
            if (_canMove)
                _movement = _moveDir * Time.deltaTime;
            else
                _movement = Vector3.zero;

            //--> Make the character look in the direction of movement.
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(_moveDir), _rotationSmooth);
        }

        //--> Move character
        _characterController.Move(_movement);
    }



    //--> Calculate the normalized vectors representing the horizontal direction in which the camera faces, ignoring the vertical tilt.
    private void CameraDirection()
    {
        _camForward = _camera.forward.normalized;
        _camForward.y = 0;

        _camRight = _camera.right.normalized;
        _camRight.y = 0;
    }



    //--> Jogging animation
    private void MovementAnimation()
    {
        _animator.SetFloat("VelX", _moveAxis.x);
        _animator.SetFloat("VelY", _moveAxis.z);
    }
}

