using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadManager : MonoBehaviour
{
    public static SceneLoadManager Instance { get; private set; }


    //--> PROPERTIES
    public Canvas Canvas { get; set; }
    public Animator Animator { get; set; }


    [Header("VARIABLES")]
    [SerializeField] private float transitionTime = 1f;


    private void Awake()
    {
        //--> Ensures that there is only one instance of this object.
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }


        //--> References.
        Animator = GetComponentInChildren<Animator>();
        Canvas = GetComponent<Canvas>();
    }



    //--> Load the next scene.
    public void LoadNextScene()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        StartCoroutine(LoadScene(nextSceneIndex));
        // Debug.Log("Click");
    }
    



    //--> Fade effect between scene changes
    public IEnumerator LoadScene(int sceneIndex)
    {
        Canvas.sortingOrder = 2;
        Animator.SetTrigger("FadeIn");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(sceneIndex);
    }
}
