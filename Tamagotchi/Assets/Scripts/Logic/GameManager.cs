using System.Collections;
using UnityEngine;


public sealed class GameManager : MonoBehaviour
{
    [Header("EVENT")]
    private bool isSubscribedToDogEvents = true;


    [Header("HEALTH")]
    private float _healthMax;
    private int _totalHealth = 3;
    private float _healthCurent;
    private float _healthCurentUI;


    [Header("LOVE")]
    [SerializeField] private float timeLostLove = 2.5f;


    [Header("PLAY")]
    [SerializeField] private float timeLostPlay = 2f;


    [Header("FEED")]
    [SerializeField] private float timeLostFood = 3f;


    [Header("REFERENCES")]
    [SerializeField] private UIManager uiManager;
    [SerializeField] private PlayerController playerController;
    [SerializeField] private Ball ball;
    private DogStatus loveStatus;
    private DogStatus playStatus;
    private DogStatus foodStatus;
    private DogEventManager dog;





    private void Awake()
    {
        //--> Instances
        dog = new DogEventManager();
        loveStatus = new DogStatus(50);
        playStatus = new DogStatus(50);
        foodStatus = new DogStatus(50);
    }



    private void Start()
    {
        //--> FadeOut animation at startup.
        SceneLoadManager.Instance.Animator.SetTrigger("FadeOut");

        //--> Changing the sort order of the SceneLoadManager canvas.
        SceneLoadManager.Instance.Canvas.sortingOrder = 0;


        //--> Initialize variables
        _healthMax = uiManager.Love.Length + uiManager.Play.Length + uiManager.Feed.Length;
        _healthCurent = _healthMax;
        _healthCurentUI = _healthMax / _totalHealth;
    }



    //--> Resumes subscription to the dog's events and registers the action.
    public void SubscribeFromDogEvents()
    {
        Debug.Log("Subscribed to the event.");
        dog.StateChanged += OnDogStateChanged;
        isSubscribedToDogEvents = true;
    }



    //--> Method for unsubscribing from the event.
    public void UnsubscribeFromDogEvents()
    {
        Debug.Log("Unsubscribe to the event.");
        dog.StateChanged -= OnDogStateChanged;
        isSubscribedToDogEvents = false;
    }



    //--> Handles the dog's status change event and records the new status in the console.
    private void OnDogStateChanged(Enums.DogState newState)
    {
        if (isSubscribedToDogEvents)
        {
            Debug.Log("Current status: " + newState);
        }
    }



    //--> Pet the dog by clicking on the StrokeButton in Unity.
    public void OnPointerClickLove()
    {
        RecoverLove();
        dog.Love();
    }



    //--> Play with the dog by clicking on the FunButton in Unity.
    public void OnPointerClickPlay()
    {
        RecoverPlay();
        dog.Play();
    }



    //--> Feed the dog by clicking on the FoodButton in Unity.
    public void OnPointerClickFeed()
    {
        RecoverFeed();
        dog.Feed();
    }






    //--> Method to start the game
    public void StartGame()
    {
        //--> Initiate correutines to periodically disable the dog's needs.
        StartCoroutine(DisableteLovePeriodically());
        StartCoroutine(DisabletePlayPeriodically());
        StartCoroutine(DisableteFoodPeriodically());

        //--> Enable character movement.
        playerController.CanMove = true;

        //-> If you do not enter a name for the dog, it is assigned by default as Dog.
        if (uiManager.PetName.text == "")
        {
            uiManager.PetName.text = "Dog";
        }
    }






    //--> Calls the method that stipulates the retrieval of the assigned dog value and updates the UI.
    private void RecoverLove()
    {
        loveStatus.Recover();
        uiManager.ActivateLove(loveStatus.NeedCurrent);
        RecoverHealth();
    }



    //--> Call the method that manages the dog's loss of love and update the UI.
    private void LostLove()
    {
        loveStatus.Lost();
        uiManager.DeactivateLove(loveStatus.NeedCurrent);
        LostHealth();
    }



    //--> Deactivates the assigned value of the dog periodically.
    private IEnumerator DisableteLovePeriodically()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeLostLove);
            LostLove();
        }
    }






    //--> Calls the method that manages the retrieval of the assigned dog value and updates the UI.
    private void RecoverPlay()
    {
        playStatus.Recover();
        uiManager.ActivatePlay(playStatus.NeedCurrent);
        RecoverHealth();
        StartCoroutine(ball.BallThrowing());
    }



    //--> Call the method that manages the dog's loss of love and update the UI.
    private void LostPlay()
    {
        playStatus.Lost();
        uiManager.DeactivatePlay(playStatus.NeedCurrent);
        LostHealth();
    }



    //--> Deactivates the assigned value of the dog periodically.
    private IEnumerator DisabletePlayPeriodically()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeLostPlay);
            LostPlay();
        }
    }






    //--> Calls the method that manages the retrieval of the assigned dog value and updates the UI.
    private void RecoverFeed()
    {
        foodStatus.Recover();
        uiManager.ActivateFood(foodStatus.NeedCurrent);
        RecoverHealth();
    }



    //--> Call the method that manages the dog's loss of love and update the UI.
    private void LostFeed()
    {
        foodStatus.Lost();
        uiManager.DeactivateFood(foodStatus.NeedCurrent);
        LostHealth();
    }



    //--> Deactivates the assigned value of the dog periodically.
    private IEnumerator DisableteFoodPeriodically()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeLostFood);
            LostFeed();
        }
    }






    //--> Calls the method that manages the retrieval of the assigned health value and updates the UI.
    private void RecoverHealth()
    {
        if (_healthCurentUI == _healthMax / _totalHealth)
        {
            return;
        }

        _healthCurent = loveStatus.NeedCurrent + playStatus.NeedCurrent + foodStatus.NeedCurrent;
        _healthCurentUI = _healthCurent / _totalHealth;

        //--> Only increase health when it is an integer.
        if (_healthCurentUI % 1 == 0)
        {
            uiManager.ActivateHealth((int)_healthCurentUI);
        }
    }



    //--> Call the method that manages the dog's loss of health and update the UI.
    private void LostHealth()
    {
        if (_healthCurentUI <= 0)
        {
            return;
        }

        _healthCurent = loveStatus.NeedCurrent + playStatus.NeedCurrent + foodStatus.NeedCurrent;
        _healthCurentUI = _healthCurent / _totalHealth;

        //--> Only deduct health when it is an integer.
        if (_healthCurentUI % 1 == 0)
        {
            uiManager.DeactivateHealth((int)_healthCurentUI);
        }
    }






    //--> Subscribe to StateChanged dog event.
    private void OnEnable()
    {
        SubscribeFromDogEvents();
    }



    //--> Unsubscribe to StateChanged dog event.
    private void OnDisable()
    {
        UnsubscribeFromDogEvents();
    }
}
