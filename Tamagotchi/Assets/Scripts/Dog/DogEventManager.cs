using System;

public class DogEventManager
{
    //--> Declaring a state change event using System.Action
    public event Action<Enums.DogState> StateChanged;

    //--> Almacenar el estado actual del perro.
    private Enums.DogState _currentState;





    //--> Method for petting the dog.
    public void Love()
    {
        // Debug.Log("The dog has been petted.");

        //--> Changing the dog's status.
        ChangeState(Enums.DogState.Stroke);
        
    }



    //--> Method for Playing with the dog.
    public void Play()
    {
        // Debug.Log("The dog has played.");

        //--> Changing the dog's status.
        ChangeState(Enums.DogState.Fun);
    }



    //--> Method for feeding the dog.
    public void Feed()
    {
        // Debug.Log("The dog has been fed.");

        //--> Changing the dog's status.
        ChangeState(Enums.DogState.Food);
    }



    //--> Change the internal status of the dog and notify subscribers.
    private void ChangeState(Enums.DogState newState)
    {
        if (newState != _currentState)
        {
            _currentState = newState;

            //--> Check for StateChanged event subscribers.
            if (StateChanged != null)
            {
                //--> Invoke the event and notify subscribers of the status change.
                StateChanged.Invoke(newState);
            }
        }
    }
}
