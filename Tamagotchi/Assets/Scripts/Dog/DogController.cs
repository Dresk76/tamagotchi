using UnityEngine;
using UnityEngine.AI;

public class DogController : MonoBehaviour
{
    //--> Note: You must change the Stopping Distance of the Nav Mesh Agent in Unity e.g.: 0.1.

    [Header("DESTINATION")]
    private GameObject _destinationPlayer;


    [Header("STOPPING DISTANCE")]
    [SerializeField] private float stoppingDistancePlayer = 4f;
    [SerializeField] private float stoppingDistanceDog = 0.5f;


    [Header("REFERENCES")]
    [SerializeField] private Ball ball;
    private NavMeshAgent _navMeshAgent;
    private Animator _animator;




    private void Awake()
    {
        _destinationPlayer = GameObject.FindGameObjectWithTag("Player");
        _navMeshAgent = GetComponentInChildren<NavMeshAgent>();
        _animator = GetComponentInChildren<Animator>();
    }



    private void Update()
    {
        Move();
        LogicAnimation();
    }



    //--> Moving the dog with navmesh;
    private void Move()
    {
        //--> Move the dog towards the character when the ball has not been thrown.
        if (ball.CanLaunch)
        {
            _navMeshAgent.SetDestination(_destinationPlayer.transform.position);
            _navMeshAgent.stoppingDistance = stoppingDistancePlayer;
        }


        //--> Move the dog towards the ball when it has been launched.
        if (ball.BallPrefab != null)
        {
            if (!ball.CanLaunch)
            {
                _navMeshAgent.SetDestination(ball.BallPrefab.transform.position);
                _navMeshAgent.stoppingDistance = stoppingDistanceDog;
            }
        }
    }



    //--> Jogging animation
    private void LogicAnimation()
    {
        _animator.SetFloat("Speed", _navMeshAgent.velocity.magnitude);
    }
}
