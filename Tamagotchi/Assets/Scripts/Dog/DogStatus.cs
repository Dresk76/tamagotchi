using System.Collections;
using UnityEngine;

public class DogStatus
{
    public int MaxNeed { get; set; }
    public int NeedCurrent { get; set; }



    public DogStatus(int maxNeed)
    {
        MaxNeed = maxNeed;
        NeedCurrent = MaxNeed;
    }



    //--> Manages the recovery of the dog's assigned need.
    public void Recover()
    {
        int NeedRandom = 0;

        //--> Check if the maximum allowable requirement has already been given.
        if (NeedCurrent == MaxNeed)
        {
            return;
        }

        //--> Controls the cases of the last and penultimate increment to avoid exceeding the limit when generating random numbers.
        if (NeedCurrent == (MaxNeed - 2))
        {
            NeedRandom = Random.Range(1, 3);
        }
        else if (NeedCurrent == (MaxNeed - 1))
        {
            NeedCurrent += 1;
        }
        else
        {
            NeedRandom = Random.Range(1, 4);
        }

        //--> Add the calculated need.
        NeedCurrent += NeedRandom;
    }



    //--> Manages the loss of the dog's assigned need.
    public void Lost()
    {
        //--> Verifica si ya perdio toda la necesidad.
        if (NeedCurrent <= 0)
        {
            return;
        }

        //--> Reduces the calculated need.
        NeedCurrent -= 1;
    }
}
